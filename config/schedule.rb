set :environment, "production"
set :output, {:error => "log/cron_error_log.log", :standard => "log/cron_log.log"}

every 1.day do
  rake "results:parse_data"
end

every 1.day do
  rake "fixtures:parse_fixtures"
end