require 'test_helper'

class FixtureControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael)
  end
  
  test "should get index" do
    log_in_as(@user)
    get fixtures_path
    assert_response :success
  end

end
