# The Football App

This web application was created as part of the continuous assessment for course CS2012; Advanced Web Application Development. 

The website collects open-source data of the latest results from the English Premier League and based on this data, predicts the results of upcoming fixtures. 

The website is located at [The Football App](https://awad-football-results.herokuapp.com/)

## Created by

* Kanat Kusembaev
* Gareth Donaldson
* Miroslav Stoyanov
