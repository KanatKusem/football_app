class CreateFixtures < ActiveRecord::Migration[5.1]
  def change
    create_table :fixtures do |t|
     
      t.string :Date
      t.string :HomeTeam
      t.string :AwayTeam

      t.timestamps
    end
  end
end
