class AddDivToFixture < ActiveRecord::Migration[5.1]
  def change
    add_column :fixtures, :div, :string
  end
end
