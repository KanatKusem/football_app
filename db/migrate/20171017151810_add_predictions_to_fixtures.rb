class AddPredictionsToFixtures < ActiveRecord::Migration[5.1]
  def change
    add_column :fixtures, :predict_goals_home, :integer
    add_column :fixtures, :predict_goals_away, :integer
  end
end