class ResultsController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user, only: :destroy
  
  def index
    if !logged_in?
      flash[:warning] = "Please log in to access results."
      redirect_to login_path
    else
        @results = Result.all
    end
  end
  
  def show
    @line = Result.find(params[:id])
    @team = @line.HomeTeam
    score_h = score_a = conceded_a = conceded_h = 0
    Result.where(HomeTeam: @team).find_each do |goal|
      score_h += goal.FTHG
      conceded_h += goal.FTAG
    end
    Result.where(AwayTeam: @team).find_each do |goal|
      score_a += goal.FTAG
      conceded_a += goal.FTHG
    end
    @scored = score_a + score_h
    @conceded = conceded_a + conceded_h
  end
end