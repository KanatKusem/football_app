class CreateResults < ActiveRecord::Migration[5.1]
  def change
    create_table :results do |t|
      t.text :Date
      t.text :HomeTeam
      t.text :AwayTeam
      t.integer :FTHG
      t.integer :FTAG

      t.timestamps
    end
  end
end
