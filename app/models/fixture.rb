class Fixture < ApplicationRecord
  belongs_to :results, optional: true
end