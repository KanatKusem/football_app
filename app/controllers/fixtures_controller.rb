class FixturesController < ApplicationController
  before_action :logged_in_user, only: [:index]

  def index
  	if !logged_in?
      flash[:warning] = "Please log in to access predictions."
      redirect_to login_path
    else
      @fixtures = Fixture.all #shows the list of upcoming matches
      @fixtures.each do |fixture|
        prediction(fixture)
      end
    end
  end
  
  def prediction(fixture)
    team_home = fixture.HomeTeam
    team_away = fixture.AwayTeam

    # calculate average goals scored & conceded by home team
    home_avg_goal = Result.where(HomeTeam: team_home).average(:FTHG).round
    home_avg_conceded = Result.where(HomeTeam: team_home).average(:FTAG).round

    #calculating average goals scored & conceded by away team
    away_avg_goal = Result.where(AwayTeam: team_away).average(:FTAG).round
    away_avg_conceded = Result.where(AwayTeam: team_away).average(:FTHG).round
          
    # Update table with predictions
    fixture.predict_goals_home = home_avg_goal * away_avg_conceded
    fixture.predict_goals_away = away_avg_goal * home_avg_conceded
  end
  
end
