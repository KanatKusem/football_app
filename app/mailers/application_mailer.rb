class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@footballapp.com'
  layout 'mailer'
end
