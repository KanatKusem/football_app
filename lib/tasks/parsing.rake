# needs gem 'rake' to be insstalled
# needs Result model to be created and db:migrate executed
# to run task in command line write: rake results:parse_data

require 'csv'

namespace :results do
  desc "Parsing CSV file from website and store it to DB"
  task parse_data: [:environment] do
    ActiveRecord::Base.connection.execute("TRUNCATE TABLE results")
    csv_text = open('https://www.footballwebpages.co.uk/fixtures-results.csv?comp=1')
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      if (row['Status'] == "FT" )
        Result.create!(:Date => row['Date'], :HomeTeam => row['Home Team'], 
          :AwayTeam => row['Away Team'], :FTAG => row['A Score'], :FTHG => row['H Score'])
      end
    end
    puts "#{Time.now} - Success!"
  end
end

namespace :fixtures do
  desc "Parsing CSV file from website and store it to DB"
  task parse_fixtures: [:environment] do
    ActiveRecord::Base.connection.execute("TRUNCATE TABLE fixtures")
    csv_text = open('https://www.footballwebpages.co.uk/fixtures-results.csv?comp=1')
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      unless (row['Status'] == "FT" )
        Fixture.create!(:Date => row['Date'], :HomeTeam => row['Home Team'], 
        :AwayTeam => row['Away Team'], :div => 'EPL')
      end
    end
    puts "#{Time.now} - Success!"
  end
end
